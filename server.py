from threading import Thread
from http.server import SimpleHTTPRequestHandler, HTTPServer
import argparse
import ssl
import os

PATH_PREFIX = ""


def parse_arguments():
    parser = argparse.ArgumentParser(description='Start an HTML file browser.')
    parser.add_argument("-p", "--port", dest="port", type=int, default=8080,
                        help='port to use')
    parser.add_argument('--path_prefix', dest='path_prefix', default="",
                        help='Prefix of the server')
    return parser.parse_args()


class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        # Ensure that the user will fall to the correct prefix
        if self.path == "/":
            self.path = PATH_PREFIX

        directory = self.path.replace(PATH_PREFIX, "")
        while len(directory) > 0 and directory[0] == "/":
            directory = directory[1:]
        print(directory)
        super().do_GET()
        if self.path.endswith("/kill_me_please/"):
            print("Shutting down")
            self.server.running = False


class MainServer:
    def __init__(self, host, port):
        self._server = HTTPServer((host, port), MyHandler)

        # Add SSL
        sslcontext = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        sslcontext.load_cert_chain(
            keyfile=os.environ["HOME"] + "/.config/euler/jupyterhub/jupyter.key",
            certfile=os.environ["HOME"] + "/.config/euler/jupyterhub/jupyter.crt"
        )
        self._server.socket = sslcontext.wrap_socket(
            self._server.socket, server_side=True)

        # Start threads
        self._thread = Thread(target=self.run)
        self._thread.deamon = True
        print(f"Server is running on port {port}")

    def run(self):
        self._server.running = True
        while self._server.running:
            self._server.handle_request()

    def start(self):
        self._thread.start()

    def shut_down(self):
        print("Shutting down")
        self._thread.close()


if __name__ == "__main__":
    args = parse_arguments()
    PATH_PREFIX = args.path_prefix
    server = MainServer(host="0.0.0.0", port=args.port)
    server.start()
